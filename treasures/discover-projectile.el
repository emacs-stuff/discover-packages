(require 'projectile)
(require 'discover)

(defun discover-projectile-browse-url (url)
  (interactive)
  (browse-url "https://github.com/bbatsov/projectile"))

(discover-add-context-menu
 :context-menu '(projectile
              (description "python's virtual environment manager")
              (actions
               ;; q is for quit
               ("projectile"
                ("a" "switch to other extension" projectile-find-other-file)
                ("d" "find dir" projectile-find-dir)
                ("D" "open root" projectile-dired)
                ("e" "recentf" projectile-recentf)
                ("E" "most recent file" projectile-project-buffers-other-buffer)
                ("f" "find file" projectile-find-file)
                ("F" "find file in directory" projectile-find-file-in-directory)
                ("g" "global mode" projectile-global-mode)
                ("i" "ibuffer" projectile-ibuffer)
                ("o" "occur" projectile-multi-occur)
                ("p" "switch project" projectile-switch-project)
                ("r" "replace" projectile-replace)
                ("S" "save all buffers" projectile-save-project-buffers)
                ("t" "find test file" projectile-find-test-file)
                ("T" "toggle test/implementation" projectile-toggle-between-implementation-and-test)
                ("v" "vc-dir" projectile-vc)
                ("!" "run shell at root" projectile-run-shell-command-in-root)
                (">" "see on github" discover-projectile-browse-url)
                ))
              )
 :bind "s-d p")

(defalias 'discover-projectile 'makey-key-mode-popup-projectile)
