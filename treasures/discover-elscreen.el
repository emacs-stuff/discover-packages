;; discover elscreen commands with a magit-like menu
;; thx to https://github.com/mickeynp/discover.el

;; global key: M-f (TODO: define better key)

(require 'elscreen)
(require 'discover)

(discover-add-context-menu
 :context-menu '(elscreen
              (description "elscreen")
              ;; (lisp-switches
               ;; ("-cf" "Case should fold search" case-fold-search t nil))
              ;; (lisp-arguments
               ;; ("=l" "context lines to show (occur)"
                ;; "list-matching-lines-default-context-lines"
                ;; (lambda (dummy) (interactive) (read-number "Number of context lines to show: "))))
              (actions
               ("elscreen"
                ("c" "create new screen" elscreen-create)
                ("k" "kill screenn" elscreen-kill)
                ("K" "kill screenn and buffers" elscreen-kill-screen-and-buffers)
                ("n" "next screen" elscreen-next)
                ("p" "previous screen" elscreen-previous)
                )
))
 :bind "s-d e"
)

;; (add-hook 'after-init-hook 'discover--turn-on-elscreen) ;; unknown O_o
;; (add-hook 'after-init-hook (lambda () (define-key global-map (kbd "s-c e") 'makey-key-mode-popup-elscreen)))
;; One can bind a key to
;; (makey-key-mode-popup-elscreen)
(defalias 'discover-elscreen 'makey-key-mode-popup-elscreen)
