(require 'discover)
(require 'xcscope)
;; (require 'helm-cscope)

(discover-add-context-menu
 :context-menu '(cscope
              (description "find function calls, definitions, etc.")
              (actions
               ("cscope"
                ("g" "find global definition" helm-cscope-find-global-definition)
                ("s" "find symbol" helm-cscope-find-symbol)
                )
))
 :bind "s-d c"
 :mode 'prog-mode
 :mode-hook 'prog-mode-hook
)

(defalias 'discover-cscope 'makey-key-mode-popup-cscope)
