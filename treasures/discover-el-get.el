
(require 'discover)
(require 'el-get)

(discover-add-context-menu
 :context-menu '(el-get
              (description "manage packages through el-get.")
              (actions
               ("el-get"
                ("i" "install a package" el-get-install)
                ("r" "remove a package" el-get-remove)
                ("u" "update a package" el-get-update)
                ("U" "update all" el-get-update-all)
                ("s" "self-update" el-get-self-update)
                ("l" "list packages" el-get-list-packages)
                ("d" "describe a package" el-get-describe)
                ("e" "emacswiki-refresh" el-get-emacswiki-refresh)
                ("f" "find a recipe file" el-get-find-recipe-file)
                ("m" "make recipes" el-get-make-recipes)
                ("I" "init" el-get-init)
                ("v" "version" el-get-version)
                )
))
 :bind "s-d g"
)

(defalias 'discover-el-get 'makey-key-mode-popup-el-get)

;; (add-hook 'prog-mode-hook 'discover--turn-on-el-get)
