(require 'discover)

(discover-add-context-menu
 :context-menu '(discover
              (description "discover discoverable packages")
              (actions
               ("discover"
                ("c" "cscope" discover-cscope)
                ("e" "elscreen" discover-elscreen)
                ("f" "yafolding" discover-yafolding)
                ("g" "el-get" discover-el-get)
                ("r" "rope" discover-rope)
                ("s" "string-inflection" discover-string-inflection)
                ("v" "venv" discover-venv)
                ("p" "projectile" discover-projectile)
                )
))
 :bind "s-d d")

(defalias 'discover-discover 'makey-key-mode-popup-discover)
