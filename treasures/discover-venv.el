(require 'virtualenvwrapper)
(require 'discover)

(discover-add-context-menu
 :context-menu '(virtualenvwrapper
              (description "python's virtual environment manager")
              (actions
               ("virtualenvwrapper"
                ("w" "workon" venv-workon)
                ("d" "deactivate" venv-deactivate)
                ("m" "make virtual env" venv-mkvirtualenv)
                ("s" "shell command to all" venv-allvirtualenv-shell-command)
                ("c" "cd" venv-cdvirtualenv)
                ("y" "copy" venv-cpvirtualenv)
                ("r" "remove" venv-rmvirtualenv)
                ))
              )
 :bind "s-d v"
)

(defalias 'discover-venv 'makey-key-mode-popup-virtualenvwrapper)
