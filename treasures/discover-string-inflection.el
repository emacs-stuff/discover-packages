(require 'string-inflection)
(require 'discover)

(discover-add-context-menu
 :context-menu '(string-inflection
              (description "Toggle string case.")
              (actions
               ("string-inflection"
                ("c" "lower camelize" string-inflection-lower-camelize)
                ("C" "camelize" string-inflection-camelize)
                ("u" "underscore" string-inflection-underscore)
                ("U" "upcase" string-inflection-upcase)
                ("t" "toggle" string-inflection-toggle)
                ("y" "cycle" string-inflection-cycle)
                ))
              )
 :bind "s-d s"
 :mode 'prog-mode
 :mode-hook 'prog-mode-hook
)

(defalias 'discover-string-inflection 'makey-key-mode-popup-string-inflection)
