(require 'nose)
(require 'discover)

(discover-add-context-menu
 :context-menu '(nose
              (description "run unit tests with nosetests")
              (actions
               ("nosetests"
                ("o" "run one test" nosetests-one)
                ("a" "run all" nosetests-all)
                ))
              )
 :bind "s-d f"
 :mode 'prog-mode
 :mode-hook 'prog-mode-hook
)

(defalias 'discover-nose 'makey-key-mode-popup-nose)
