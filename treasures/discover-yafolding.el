(require 'yafolding)
(require 'discover)

(discover-add-context-menu
 :context-menu '(yafolding
              (description "folding based on indentation")
              (actions
               ("yafolding"
                ("h" "hide element" yafolding-hide-element)
                ("s" "show element" yafolding-show-element)
                ("t" "toggle element" yafolding-toggle-element)
                ("H" "hide all" yafolding-hide-all)
                ("S" "show all" yafolding-show-all)
                ("T" "toggle all" yafolding-toggle-all)
                ("p" "go parent element" yafolding-go-parent-element)
                ("P" "hide parent element" yafolding-hide-parent-element)
                ("m" "mode" yafolding-mode)
                ))
              )
 :bind "s-d f"
 :mode 'prog-mode
 :mode-hook 'prog-mode-hook
)

(defalias 'discover-yafolding 'makey-key-mode-popup-yafolding)
