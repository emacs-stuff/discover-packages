(require 'discover)
(require 'pymacs)                       ; + (pymacs-load "ropemacs" "rope-") ?

(discover-add-context-menu
 :context-menu '(rope
              (description "rope")
              (actions
               ("rope"
                ("r" "rename" rope-rename)
                ("u" "undo" rope-undo)
                ("m" "move" rope-move)
                ("s" "change signature" rope-change-signature)
                ("d" "show doc" rope-show-doc)
                )
))
 :bind "s-d r"
 :mode 'python-mode
 :mode-hook 'python-mode-hook
)

;; (add-hook 'after-init-hook 'discover--turn-on-rope) ;; unknown
;; One can bind a key to
;; (makey-key-mode-popup-rope)
(defalias 'discover-rope 'makey-key-mode-popup-rope)
